#TO IMPLEMENT

- Window reparationFormWindow
- Controller : add new controller for new windows.
- Add threads to the communication class in order to unfreeze the GUI when a socket is being created. (if the server is down, the GUI will freeze while trying to create a new socket)


#ACTUAL BUILD

Structure :

- View : Contains all the different window : LoginWindow
- Controller : Contains all the different controller.
- Model : Contains all the model for creating objects from the database.
- JsonModel : Contains all Json processing methods
- CommunicationModel(Controller) : All related socket classes

When the user fill all the fields and click on "Connect" a JsonObject will be sent to the server. The client will be waiting an answer to show the next window (to implement)

Structure JSON : 


```
#!json
{
 "Action" : "<action>",
 "<key_for_this_action>" : "<value>",
 "<key_for_this_action>" : "<value>",
 "<key_for_this_action>" : "<value>",
 ...
}
```

Example of json for connection from client :


```
#!json
{
 "Action" : "Connection",
 "ConnectionState" : "Try",
 "Login" : "<login>",
 "Pwd" : "<password>"
}
```

And json of response of server for connection :
```
#!json
{
 "Action" : "Connection",
 "ConnectionState" : "Error",
 "Message" : "<error message>"
}
```

#Description

## controller ##

We decide to make one controller per View. Every view has his own controller as attribute. It will be call to decide which action will be execute.

EXEMPLE : connectWindow.java and controllerConnectWindow.java

The controller is called when we try to connect an user and decide which menu to open.

```
#!java
public class connectWindow extends JFrame implements ActionListener, MouseListener {
  controllerConnectWindow controller = new controllerConnectWindow();

 //....

  @Override
  public void actionPerformed(ActionEvent ae) {
  Object source = ae.getSource();

  //...
     } else {
         JsonObject json = JSONStructure.createJsonLogin(loginString, passwordString);
         boolean answer = controller.openMenuWindow(json);
         if (answer) {
             this.dispose();
         } else {    
          //....
      }
// ....
  }
}

```


```
#!java
public class controllerConnectWindow {
  public boolean openMenuWindow(JsonObject json) throws UnknownHostException, IOException {
     boolean bool = false;
     Communication com = new Communication();
     com.sendData(json);
     JsonObject inputJson = com.getData();
     String stateCom = inputJson.getString("State");
     switch(stateCom){
       case "succes":
          bool = true;
      //.....
     }
```