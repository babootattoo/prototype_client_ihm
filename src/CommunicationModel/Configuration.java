package CommunicationModel;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * This class will be used to create a configuration object. This object will contain the server application IP and the host IP
 * @author Stephane.Schenkel
 */
public class Configuration {
    
    /**
     * declaration of attribute
     * @param SERVER_IP String
     * @param HOST_IP String
     * @param SERVER_PORT int
     */
    public String SERVER_IP;
    public String HOST_IP;
    public int SERVER_PORT;
    
    /**
     * This is the constructor of configuration,
     * and it call init method
     */
    public Configuration(){
        try{
            init();
        }catch(UnknownHostException e){
            System.exit(1);
        }
    }
    
    /**
     * This method initialize attribut 
     * @throws UnknownHostException 
     */
    private void init() throws UnknownHostException{
        SERVER_IP = "10.10.10.177";
        InetAddress IP = InetAddress.getLocalHost();
        HOST_IP = IP.getHostAddress();
        SERVER_PORT = 10080;
    }
}
