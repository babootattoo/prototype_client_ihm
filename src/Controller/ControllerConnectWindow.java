/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.User;
import Model.UserDAO;
import View.ReparatorMenuWindow;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import javax.json.JsonObject;

/**
 *
 * @author charles.santerre
 */
public class ControllerConnectWindow {
    /**
     * Declaration of attribute
     * @param errorMessage String
     */
    String errorMessage = "";

    public ControllerConnectWindow() {
    }

    /**
     * This method load adapted view witch depend of type user in JsonObject
     * @param login
     * @param password
     * @return String of result state in Json
     * @throws UnknownHostException
     * @throws IOException
     * @throws ParseException 
     */
    public String openMenuWindow(String login, String password) throws UnknownHostException, IOException, ParseException {
        String answer = "";
        if (login.equals("") || password.equals("")) {
            answer = "empty";
        } else {
            JsonObject inputJson = UserDAO.tryLogin(login, password);
            String stateCom = inputJson.getString("State");

            switch (stateCom) {
                case "Success":
                    answer = "success";
                    User userLogged = User.deserialize(inputJson.getString("UserLogged"));
                    switch (userLogged.getTypeuser()) {
                        case "reparateur":
                            new ReparatorMenuWindow(userLogged);
                            break;
                        case "administrateur":
                            //new administratorMenuWindow();
                            break;
                        case "technicien":
                            //new technicianMenuWindow();
                            break;
                    }
                    break;
                case "Error":
                    answer = "wrong";
                    errorMessage = inputJson.getString("ErrorMessage");
                    break;
            }
        }
        return answer;
    }
    
    /**
     * Return error message situation of adapted
     * @return String message
     */
    public String getErrorMessage(){
        return this.errorMessage;
    }
}
