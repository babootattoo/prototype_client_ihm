package Controller;

import Model.Car;
import Model.DateFormatted;
import Model.VehicleDAO;
import View.ListVehicleWaitingWindow;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author Charles.Santerre, Brice.Boutamdja
 */
public class ControllerListVehicleWindow {
    /**
     * Declaration of attribute
     * @param errorMessage String
     * @param lCar List of Car object
     */
    String errorMessage = "";
    List<Car> lCar = new ArrayList<Car>();

    public ControllerListVehicleWindow() {
    }
    
    /**
     * It updates a car, which send in paramater, from the list and return it
     * @param id Int
     * @param matriculation String
     * @param purchaseDate DateFormatted
     * @param lastRevision DateFormatted
     * @return List of Car object
     * @throws IOException
     * @throws ParseException 
     */
    public List<Car> updateVehicle(int id, String matriculation, DateFormatted purchaseDate, DateFormatted lastRevision) throws IOException, ParseException{
        JsonObject inputJson = VehicleDAO.updateVehicleWaitingForReparation(new Car(id, matriculation, purchaseDate, lastRevision));
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                choiceListVehicleWaiting();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return lCar;
    }

    /**
     * It deletes a car, which send in paramater, from the list and return it
     * @param aCar Car Object
     * @return List of Car object
     * @throws IOException
     * @throws ParseException 
     */
    public List<Car> deleteVehicle(Car aCar) throws IOException, ParseException{
        JsonObject inputJson = VehicleDAO.deleteVehicleWaitingForReparation(aCar);
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                choiceListVehicleWaiting();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return lCar;
    }

    /**
     * It adds a car, which send in paramater, from the list and return it
     * @param Matriculation String
     * @param PurchaseDate DateFormatted
     * @param LastRevision DateFormatted
     * @return List of Car object
     * @throws IOException
     * @throws ParseException 
     */
    public List<Car> addVehicle(String Matriculation, DateFormatted PurchaseDate, DateFormatted LastRevision) throws IOException, ParseException{
        JsonObject inputJson = VehicleDAO.addVehicleWaitingForReparation(Matriculation,PurchaseDate,LastRevision);
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                choiceListVehicleWaiting();
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        
        return lCar;
    }
    
    /**
     * It creates list of cars, it use for opening listVehicleWaitingWindow
     * @throws IOException
     * @throws ParseException 
     */
    private void choiceListVehicleWaiting() throws IOException, ParseException {
        List<Car> listCar= new ArrayList<Car>();
        JsonObject inputJson = VehicleDAO.getListVehicleWaitingForReparation();
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                JsonArray JArray = inputJson.getJsonArray("ListCar");
                for(int i = 0; i < JArray.size(); i++){
                    JsonObject jsonCar = JArray.getJsonObject(i);
                    Car aCar = Car.deserialize(jsonCar.getString("Car"+i));
                    listCar.add(aCar);
                }
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        this.lCar = listCar;
    }
    
    /**
     * Return error message situation of adapted
     * @return String message
     */
    public String getErrorMessage(){
        return this.errorMessage;
    }
}
