/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Car;
import Model.ReparationForm;
import Model.ReparationFormDAO;
import Model.VehicleDAO;
import View.ListVehicleWaitingWindow;
import View.ReparationFormWindow;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 *
 * @author charles.santerre
 */
public class ControllerReparatorMenuWindow {
    
    /**
     * Declaration of attribute
     * @param errorMessage String
     */
    String errorMessage = "";

    public ControllerReparatorMenuWindow() {

    }

    /**
     * This method return boolean compared to state of list vehicule waiting
     * @return boolean 
     * @throws IOException 
     */
    public boolean choiceListVehicleWaiting() throws IOException, ParseException {
        boolean bool = false;
        List<Car> lCar= new ArrayList<Car>();
        JsonObject inputJson = VehicleDAO.getListVehicleWaitingForReparation();
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                bool = true;
                JsonArray JArray = inputJson.getJsonArray("ListCar");
                for(int i = 0; i < JArray.size(); i++){
                    JsonObject jsonCar = JArray.getJsonObject(i);
                    Car aCar = Car.deserialize(jsonCar.getString("Car"+i));
                    lCar.add(aCar);
                }
                new ListVehicleWaitingWindow(lCar);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return bool;
    }
    
    /**
     * This method load view of reparationForm with object ReparationForm give on param
     * @return object ReparationForm
     * @throws IOException
     * @throws ParseException 
     */
    public ReparationForm choiceGiveReparationForm() throws IOException, ParseException{
        ReparationForm repForm = null;
        JsonObject inputJson = ReparationFormDAO.getReparationForm(1);
        String stateCom = inputJson.getString("State");
        switch(stateCom){
            case "Success":
                repForm = ReparationForm.deserializeAll(inputJson.getString("ReparationForm"), inputJson.getString("Car"), inputJson.getString("Bike"), inputJson.getString("Parking"));
                new ReparationFormWindow(repForm);
                break;
            case "Error":
                errorMessage = inputJson.getString("ErrorMessage");
                break;
        }
        return repForm;
    }
    
    /**
     * Return error message situation of adapted
     * @return String message
     */
    public String getErrorMessage(){
        return this.errorMessage;
    }
}
