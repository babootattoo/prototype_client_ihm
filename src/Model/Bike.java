/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.ParseException;

/**
 *
 * @author Stephane.Schenkel
 */
public class Bike {
    
    /**
     * Declaration of attribute
     * @param id Int 
     * @param purchaseDate DateFormatted
     * @param lastRevision DateFormatted
     */
    private int id;
    private DateFormatted purchaseDate; 
    private DateFormatted lastRevision; 

    public int getId() {
        return id;
    }

    public DateFormatted getPurchaseDate() {
        return purchaseDate;
    }

    public DateFormatted getLastRevision() {
        return lastRevision;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPurchaseDate(DateFormatted purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public void setLastRevision(DateFormatted lastRevision) {
        this.lastRevision = lastRevision;
    }

    public Bike(int id, DateFormatted purchaseDate, DateFormatted lastRevision) {
        this.id = id;
        this.purchaseDate = purchaseDate;
        this.lastRevision = lastRevision;
    }
    
    public static String serialize(Bike bike){
        if(bike != null){
            return bike.id + "///" + bike.purchaseDate + "///" + bike.lastRevision;
        }else{
            return "null";
        }
    }
    
    public static Bike deserialize(String bike) throws ParseException{
        if(bike.equals("null")){
            return null;
        }else{
            String[] eachAttributes = bike.split("///");
            return new Bike(Integer.parseInt(eachAttributes[0]), new DateFormatted(eachAttributes[1]), new DateFormatted(eachAttributes[2]));
        }
    }
    
}
