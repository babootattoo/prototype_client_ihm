/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.ParseException;

/**
 *
 * @author Charles.Santerre
 */
public class Car {
    
    /**
     * Declaration of attribute
     * @param id Int
     * @param matriculation String
     * @param purchaseDate DateFormatted
     * @param lastRevision DateFormatted
     */
    private int id;
    private String matriculation;
    private DateFormatted purchaseDate; 
    private DateFormatted lastRevision; 
    
    public Car(int anId, String aMatriculation, DateFormatted aPurchaseDate, DateFormatted aLastRevision){
        id = anId;
        matriculation = aMatriculation;
        purchaseDate = aPurchaseDate;
        lastRevision = aLastRevision;
    }
    
   

    public int getId() {
        return id;
    }

    public String getMatriculation() {
        return matriculation;
    }

    public DateFormatted getPurchaseDate() {
        return purchaseDate;
    }

    public DateFormatted getLastRevision() {
        return lastRevision;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMatriculation(String matriculation) {
        this.matriculation = matriculation;
    }

    public void setPurchaseDate(DateFormatted purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public void setLastRevision(DateFormatted lastRevision) {
        this.lastRevision = lastRevision;
    }
    
    
    
    public static String serialize(Car car){
        if(car != null){
            return car.id + "///" + car.matriculation + "///" + car.purchaseDate + "///" + car.lastRevision;
        }else{
            return "null";
        }
    }
    
    public static Car deserialize(String car) throws ParseException{
        if(car.equals("null")){
            return null;
        }else{
            String[] eachAttributes = car.split("///");
            return new Car(Integer.parseInt(eachAttributes[0]), eachAttributes[1], new DateFormatted(eachAttributes[2]), new DateFormatted(eachAttributes[3]));
        }
    }

    @Override
    public String toString() {
        return "Car{" + "id=" + id + ", matriculation=" + matriculation + ", purchaseDate=" + purchaseDate + ", lastRevision=" + lastRevision + '}';
    }
    
}
