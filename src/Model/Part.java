/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Charles.Santerre
 */
public class Part {
    
    /**
     *
     * Declaration of attribute
     * 
     * @param id Int
     * @param stock Int
     * @param description String
     * @param purchasePrice Double
     */
    private int id;
    private int stock;
    private String description;
    private Double purchasePrice;
    
    

    public int getId() {
        return id;
    }

    public int getStock() {
        return stock;
    }

    public String getDescription() {
        return description;
    }

    public Double getPurchasePrice() {
        return purchasePrice;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPurchasePrice(Double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }
}
