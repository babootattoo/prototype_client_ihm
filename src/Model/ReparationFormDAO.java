/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import CommunicationModel.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author @author Stephane.Schenkel
 */
public class ReparationFormDAO {
    
    /**
     * This method build JsonObject for ReparationForm with JsonObjectBuilder
     * @param id Int id of ReparationForm class
     * @return JsonObject 
     */
    private static JsonObject getReparationFormRequestJson(int id){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "giveReparationForm");
        builder.add("ConnectionState", "Try");
        builder.add("IdReparationForm", id);
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect the ReparationForm by id give on param
     * @param id Int id of ReparationForm class
     * @return JsonObject
     * @throws IOException 
     */
    public static JsonObject getReparationForm(int id) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getReparationFormRequestJson(id));
        result = com.getData();
        com.close();
        return result;
    }
    
}
