/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import CommunicationModel.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author @author Stephane.Schenkel
 */
public class UserDAO {
    
    /**
     * This method return JsonObject for connection with JsonObjectBuilder
     * @param login String login of User class
     * @param password String password of User class
     * @return JsonObject
     */
    private static JsonObject getLoginRequestJson(String login, String password){
        JsonObject result = null;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "Connection");
        builder.add("ConnectionState", "Try");
        builder.add("Login", login);
        builder.add("Password", password);
        result = builder.build();
        return result;
    }
    
    /**
     * This method collect json to user give by login and password on param
     * @param login String login of User class
     * @param password String password of User class
     * @return JsonObject
     * @throws IOException 
     */
    public static JsonObject tryLogin(String login, String password) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getLoginRequestJson(login, password));
        result = com.getData();
        com.close();
        return result;
    }
    
}
