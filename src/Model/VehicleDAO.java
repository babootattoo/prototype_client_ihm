/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import CommunicationModel.Communication;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author @author Stephane.Schenkel, Charles.Santere, Brice.Boutamdja
 */
public class VehicleDAO {
    
    /**
     * This method return JsonObject for get vehicle with JsonObjectBuilder
     * @return JsonObject
     */
    private static JsonObject getListVehicleRequestJson(){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "giveListVehicleReparation");
        builder.add("ConnectionState", "Try");
        json = builder.build();
        return json;
    }
    
    /**
     * This method collect json to vehicle
     * @return JsonObject
     * @throws IOException 
     */
    public static JsonObject getListVehicleWaitingForReparation() throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(getListVehicleRequestJson());
        result = com.getData();
        com.close();
        return result;
    }
    
    /**
     * This method return JsonObject for update vehicle with JsonObjectBuilder
     * @return JsonObject
     */
    private static JsonObject updateVehicleRequestJson(Car car){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "updateVehicleReparation");
        builder.add("ConnectionState", "Try");
        builder.add("Car",Car.serialize(car));      
        json = builder.build();
        return json;        
    }

    /**
     * This method return JsonObject for delete vehicle with JsonObjectBuilder
     * @return JsonObject
     */
    private static JsonObject deleteVehicleRequestJson(Car aCar){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "deleteVehicleReparation");
        builder.add("ConnectionState", "Try");
        builder.add("Car",Car.serialize(aCar));
        json = builder.build();
        return json;        
    }
    
    /**
     * This method return JsonObject for add vehicle with JsonObjectBuilder
     * @return JsonObject
     */
    private static JsonObject addListVehicleRequestJson(String Matriculation, DateFormatted PurchaseDate, DateFormatted LastRevision){
        JsonObject json;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "addVehicleReparation");
        builder.add("ConnectionState", "Try");
        builder.add("Matriculation",Matriculation);
        builder.add("PurchaseDate",PurchaseDate.toString());
        builder.add("LastRevision",LastRevision.toString());        
        json = builder.build();
        return json;        
    }

    
    

    /**
    * This method collect json to update vehicle
    * @return JsonObject
    * @throws IOException 
    */
    public static JsonObject updateVehicleWaitingForReparation(Car aCar) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(updateVehicleRequestJson(aCar));
        result = com.getData();
        com.close();
        return result;
    }

    /**
    * This method collect json to add vehicle
    * @return JsonObject
    * @throws IOException 
    */
    public static JsonObject addVehicleWaitingForReparation(String Matriculation, DateFormatted PurchaseDate, DateFormatted LastRevision) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(addListVehicleRequestJson(Matriculation,PurchaseDate,LastRevision));
        result = com.getData();
        com.close();
        return result;
    }

    /**
    * This method collect json to delete vehicle
    * @return JsonObject
    * @throws IOException 
    */
    public static JsonObject deleteVehicleWaitingForReparation(Car aCar) throws IOException{
        JsonObject result = null;
        Communication com = new Communication();
        com.sendData(deleteVehicleRequestJson(aCar));
        result = com.getData();
        com.close();
        return result;
    }

    
}
