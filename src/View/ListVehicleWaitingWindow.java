package View;

import Controller.ControllerListVehicleWindow;
import Model.Car;
import Model.DateFormatted;
import Model.ReparationForm;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Charles.Santerre, Brice.Boutamdja
 */
public class ListVehicleWaitingWindow extends JFrame{
       JPanel panel = new JPanel();
    
    JButton returnButton = new JButton("return");
    
    ControllerListVehicleWindow controller = new ControllerListVehicleWindow();

    final DefaultListModel vehicle = new DefaultListModel();
    
    private JList list = new JList(vehicle);
    JTextField textFieldId = new JTextField(10);
    JTextField textFieldMatriculation = new JTextField(10);
    JTextField textFieldPurchaseDate = new JTextField(10);
    JTextField textFieldLastRevision = new JTextField(10);
    
    List<Car> lCar = new ArrayList<Car>();
    
    public ListVehicleWaitingWindow(List<Car> lCar){
        this.lCar = lCar;
        
        Display(lCar);
        
        /**
         * Parameters of the window
         */
        this.setSize(450,300);
        this.setTitle("Client Prototype");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new closeWindow());
        this.setLocationRelativeTo(null);

    }
    
    /**
     * display scrollpane and button on the north
     * @param lCar list of Car Object
     * @return Jcomponent
     */
    protected JComponent getList(List<Car> lCar){
        JPanel inner = new JPanel(new FlowLayout());
        JPanel setup = new JPanel(new BorderLayout());
        JPanel titlePanel = new JPanel(new FlowLayout());
        JPanel scrollPanel = new JPanel(new FlowLayout());
        JPanel buttonPanel = new JPanel(new FlowLayout());
        titlePanel.add(new JLabel("List vehicle waiting for reparation"));
        JButton selectButton = new JButton("select");
        JButton addButton = new JButton("add");
        JButton deleteButton = new JButton("delete");
        JButton updateButton = new JButton("update");        
        JButton resetButton = new JButton("reset");        
        selectButton.addMouseListener(new selectClick());
        updateButton.addMouseListener(new updateClick());
        deleteButton.addMouseListener(new deleteClick());
        resetButton.addMouseListener(new resetClick());
        addButton.addMouseListener(new addClick());
        for(int i = 0; i < lCar.size();i++){
            vehicle.addElement(Car.serialize(lCar.get(i)));
        }
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.setVisibleRowCount(3);
        JScrollPane pieceScrollPane = new JScrollPane(list);
        setup.add(titlePanel, BorderLayout.NORTH);
        scrollPanel.add(pieceScrollPane);
        buttonPanel.add(selectButton);
        buttonPanel.add(addButton);
        buttonPanel.add(deleteButton);
        buttonPanel.add(updateButton);
        buttonPanel.add(resetButton);
        setup.add(titlePanel, BorderLayout.NORTH);
        setup.add(scrollPanel, BorderLayout.CENTER);
        setup.add(buttonPanel, BorderLayout.SOUTH);
        inner.add(setup);
        return inner;
    }
    
    /**
     * Add cars into JList
     * @param lCar 
     */
    private void updateListVehicle(List<Car> lCar){
        vehicle.clear();
        for(int i = 0; i < lCar.size();i++){
            vehicle.addElement(Car.serialize(lCar.get(i)));
        }
    }
    
   class closeWindow extends WindowAdapter{
       public void windowClosing(WindowEvent e){
                int reponse = JOptionPane.showConfirmDialog(ListVehicleWaitingWindow.this,
                        "Do you want to exit application ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION)
                    ListVehicleWaitingWindow.this.dispose();
            }
   }     
        
    class selectClick extends MouseAdapter{
        public void mouseClicked(MouseEvent e){
            try {
                String data = list.getSelectedValue().toString();
                updateField(Car.deserialize(data));
            } catch (ParseException ex) {
                Logger.getLogger(ListVehicleWaitingWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    class updateClick extends MouseAdapter{
        public void mouseClicked(MouseEvent e){
            try {
                lCar = controller.updateVehicle(Integer.parseInt(textFieldId.getText()),
                        textFieldMatriculation.getText(),
                        new DateFormatted(textFieldPurchaseDate.getText()),
                        new DateFormatted(textFieldLastRevision.getText()));
                updateListVehicle(lCar);
            } catch (ParseException ex) {
                Logger.getLogger(ListVehicleWaitingWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ListVehicleWaitingWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    class deleteClick extends MouseAdapter{
        public void mouseClicked(MouseEvent e){
            try {
                lCar = controller.deleteVehicle(new Car(Integer.parseInt(textFieldId.getText()),
                        textFieldMatriculation.getText(),
                        new DateFormatted(textFieldPurchaseDate.getText()),
                        new DateFormatted(textFieldLastRevision.getText())));
                updateListVehicle(lCar);
            } catch (IOException ex) {
                Logger.getLogger(ListVehicleWaitingWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(ListVehicleWaitingWindow.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
    
    class returnClick extends MouseAdapter{
        public void mouseClicked(MouseEvent e){
            ListVehicleWaitingWindow.this.dispose();
        }
    }
    
    class resetClick extends MouseAdapter{
        public void mouseClicked(MouseEvent e){
        textFieldId.setText("");
        textFieldMatriculation.setText("");
        textFieldPurchaseDate.setText("");
        textFieldLastRevision.setText("");
        }
    }
    
    class addClick extends MouseAdapter{
        public void mouseClicked(MouseEvent e){
            try {
                String Matriculation = textFieldMatriculation.getText();
                DateFormatted PurchaseDate = new DateFormatted(textFieldPurchaseDate.getText());
                DateFormatted LastRevision = new DateFormatted(textFieldLastRevision.getText());
                lCar = controller.addVehicle(Matriculation,PurchaseDate,LastRevision);
                updateListVehicle(lCar);
            } catch (ParseException ex) {
                Logger.getLogger(ListVehicleWaitingWindow.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ListVehicleWaitingWindow.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
    
    /**
     * Display textField on center
     * @return Jcomponent
     */
    protected JComponent getGrid() {
        JLabel labelId = new JLabel("Id : ");
        JLabel labelMatriculation = new JLabel("Matriculation  : ");
        JLabel labelPurchaseDate = new JLabel("PurchaseDate : ");
        JLabel labelLastRevision = new JLabel("LastRevision : ");
        
        JPanel inner = new JPanel(new FlowLayout());
        JPanel setup = new JPanel(new BorderLayout());
        JPanel buttonPanel = new JPanel();
        JPanel labelPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
        
        labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
        
        labelId.setLabelFor(textFieldId);
        labelId.setAlignmentY(CENTER_ALIGNMENT);
        labelMatriculation.setAlignmentY(CENTER_ALIGNMENT);
        labelPurchaseDate.setAlignmentY(CENTER_ALIGNMENT);
        labelLastRevision.setAlignmentY(CENTER_ALIGNMENT);
        labelMatriculation.setLabelFor(textFieldMatriculation);
        labelPurchaseDate.setLabelFor(textFieldPurchaseDate);
        labelLastRevision.setLabelFor(textFieldLastRevision);
        textFieldId.setEditable(false);
        textFieldId.setAlignmentX(JButton.CENTER_ALIGNMENT);
        textFieldMatriculation.setAlignmentX(JButton.CENTER_ALIGNMENT);
        textFieldPurchaseDate.setAlignmentX(JButton.CENTER_ALIGNMENT);
        textFieldLastRevision.setAlignmentX(JButton.CENTER_ALIGNMENT);
        labelPanel.add(labelId);
        labelPanel.add(Box.createVerticalStrut(5));
        labelPanel.add(labelMatriculation);
        labelPanel.add(Box.createVerticalStrut(5));
        labelPanel.add(labelPurchaseDate);
        labelPanel.add(Box.createVerticalStrut(5));
        labelPanel.add(labelLastRevision);
        buttonPanel.add(textFieldId);
        buttonPanel.add(textFieldMatriculation);
        buttonPanel.add(textFieldPurchaseDate);
        buttonPanel.add(textFieldLastRevision);
        setup.add(labelPanel, BorderLayout.WEST);
        setup.add(buttonPanel, BorderLayout.EAST);
        inner.add(setup);
        return inner;   
    }
    
    /**
     * Display textField on center with element of Car Object
     * @param car Car Object
     * @return Jcomponent
     */
    protected void updateField(Car car) {
        textFieldId.setText(Integer.toString(car.getId()));
        textFieldMatriculation.setText(car.getMatriculation());
        textFieldPurchaseDate.setText(car.getPurchaseDate().toString());
        textFieldLastRevision.setText(car.getLastRevision().toString());
        setVisible(true);
    }
    
    private void Display(List<Car> lCar){
        BorderLayout panelLayout = new BorderLayout();
        panel.setLayout(panelLayout);
        
        JPanel buttonPanel = new JPanel(new FlowLayout());
        
        returnButton.addMouseListener(new returnClick());
        buttonPanel.add(returnButton);
        panel.add(getList(lCar), BorderLayout.NORTH);
        panel.add(getGrid(), BorderLayout.CENTER);
        
        
        panel.add(buttonPanel, BorderLayout.SOUTH);
        this.add(panel);
    }
}
