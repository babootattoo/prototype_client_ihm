package View;

import Model.ReparationForm;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author @author Charles., Brice.Boutamdja
 */
public class ReparationFormWindow extends JFrame{
    
    /**
     * declartion of atribute
     * @param panel JPanel of principal window
     * @param openListVehicle JButton with label "open list"
     * @param repForm Object of ReparationForm class 
     */
    JPanel panel = new JPanel();
    
    JButton returnButton = new JButton("return");

    
    ReparationForm repForm;
    
    public ReparationFormWindow(ReparationForm repForm){
        this.repForm = repForm;
        
        BorderLayout panelLayout = new BorderLayout();
        panel.setLayout(panelLayout);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(returnButton);
        returnButton.addMouseListener(new ReparationFormWindow.disconnectClick());
        panel.add(getGrid(repForm), BorderLayout.NORTH);
        panel.add(buttonPanel, BorderLayout.SOUTH);
        this.add(panel);
        
        /**
         * Parameters of the window
         */
        this.setSize(700,250);
        this.setTitle("Client Prototype");
        this.setVisible(true);
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                int reponse = JOptionPane.showConfirmDialog(ReparationFormWindow.this,
                        "Do you want to exit application ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION)
                    ReparationFormWindow.this.dispose();
            }
        });
        this.setLocationRelativeTo(null);
    }
    
        /**
         * Display textField on center
         * @return Jcomponent
         */
        protected JComponent getGrid(ReparationForm form) {
        JPanel inner = new JPanel();
        GridLayout panelLayout = new GridLayout(5,2);
        inner.setLayout(panelLayout);
        inner.add(new JLabel("id : "+ form.getId()));
        inner.add(new JLabel("entrydate : "+ form.getEntryDate()));
        inner.add(new JLabel("diagnosis : "+ form.getDiagnosis()));
        inner.add(new JLabel("description_cardSate : "+ form.getDescriptionCardState()));
        inner.add(new JLabel("id_parking : "+form.getParkingPlace().getId()));
        inner.add(new JLabel("description_urgencyDegree : "+form.getDescriptionUrgencyDegree()));
        inner.add(new JLabel("matriculation : "+form.getCar().getMatriculation()));
        inner.add(new JLabel("purchaseDate : "+form.getCar().getPurchaseDate()));
        inner.add(new JLabel("lastRevision : "+form.getCar().getLastRevision()));
        return inner;
        
    }

    class disconnectClick extends MouseAdapter {
        public void mouseClicked(MouseEvent e){
            ReparationFormWindow.this.dispose();
        }
    }
    
}
