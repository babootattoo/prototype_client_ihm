package View;

import Model.ReparationForm;
import Controller.ControllerReparatorMenuWindow;
import Model.Car;
import Model.User;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Charles.Santerre, Brice.Boutamdja
 */
public class ReparatorMenuWindow extends JFrame {

    /**
     * Declaration of atribute
     * @param userLogged Object to User class
     * @param panel JPanel of principal window
     * @param openListVehicle JButton with label "open list"
     * @param openReparationForm JButton with label "open reparation form (proto)"
     * @param disconnectButton JButton with label "disconnect"
     * @param controller Object to ControllerReparatorMenuWindow class
     */

    User userLogged;

    JPanel panel = new JPanel();

    JButton openListVehicle = new JButton("open list");
    
    JButton openReparationForm = new JButton("open reparation form (proto)");

    JButton disconnectButton = new JButton("disconnect");

    ControllerReparatorMenuWindow controller = new ControllerReparatorMenuWindow();

    public ReparatorMenuWindow(User userLogged) {
        this.userLogged = userLogged;
        BorderLayout panelLayout = new BorderLayout();
        panel.setLayout(panelLayout);

        /**
         * Create Menu Bar
        */
        JMenuBar menu= new JMenuBar();
        setJMenuBar(menu);
        JMenu tools = new JMenu("Tools");
        menu.add(tools);
        JMenu help = new JMenu("Help");
        menu.add(help);
        
        /**
         * Implement Menu Item for tools Menu
         */
        JMenuItem toolsItem1 = new JMenuItem("Options");
        tools.add(toolsItem1);
        JMenuItem toolsItem2 = new JMenuItem("About");
        help.add(toolsItem2);
        /**
         * Create panels
         */
        JPanel titlePanel = new JPanel(new FlowLayout());
        JPanel listPanel = new JPanel(new FlowLayout());
        JPanel buttonPanel = new JPanel();
        
        BoxLayout panelButtonLayout = new BoxLayout(buttonPanel, BoxLayout.Y_AXIS);
        buttonPanel.setLayout(panelButtonLayout);

        /**
         * Add JLabel name of prototype
         */
        
        titlePanel.add(new JLabel(userLogged.getFirstname() + " " + userLogged.getLastname() + " - " + userLogged.getTypeuser()));
        titlePanel.add(new JLabel("Menu"));

        /**
         * Buttons Listener
         */
        openReparationForm.addMouseListener(new openReparationFormClick());
        
        openListVehicle.addMouseListener(new openListVehicleWaitingReparationClick());

        disconnectButton.addMouseListener(new disconnectClick());

        /**
         * Buttons Center alignment
         */
        openReparationForm.setAlignmentX(JButton.CENTER_ALIGNMENT);
        openListVehicle.setAlignmentX(JButton.CENTER_ALIGNMENT);
        disconnectButton.setAlignmentX(JButton.CENTER_ALIGNMENT);
        
        /**
         * Add buttons
         */
        buttonPanel.add(openReparationForm);
        buttonPanel.add(openListVehicle);
        buttonPanel.add(disconnectButton);

        /**
         * Add panels to the window
         */
        panel.add(titlePanel, BorderLayout.NORTH);
        panel.add(buttonPanel, BorderLayout.CENTER);
        this.add(panel);

        /**
         * Parameters of the window
         */
        this.setSize(350, 250);
        this.setTitle("Client Prototype");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                int reponse = JOptionPane.showConfirmDialog(ReparatorMenuWindow.this,
                        "Do you want to exit application ?",
                        "confirmation",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (reponse==JOptionPane.YES_OPTION)
                    ReparatorMenuWindow.this.dispose();
            }
        });

        this.setLocationRelativeTo(null);
    }
    
    class openReparationFormClick extends MouseAdapter{
        public void mouseClicked(MouseEvent args){
            try{
                ReparationForm answerRepForm = controller.choiceGiveReparationForm();
                if(answerRepForm != null){
                }else{
                    JOptionPane.showMessageDialog(panel, controller.getErrorMessage(), "Search error", JOptionPane.ERROR_MESSAGE);
                }
            }catch (UnknownHostException e) {
                JOptionPane.showMessageDialog(panel, "Server is unreachable (Unknown Host)", "Connection error", JOptionPane.ERROR_MESSAGE);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(panel, e.getMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
            }catch(ParseException e){
                JOptionPane.showMessageDialog(panel, e.getMessage(), "Parsing Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    class disconnectClick extends MouseAdapter{
        public void mouseClicked(MouseEvent args){
            ReparatorMenuWindow.this.dispose();
            new ConnectWindow();
        }
    }
    
    class openListVehicleWaitingReparationClick extends MouseAdapter{
        public void mouseClicked(MouseEvent args){
            try {
                boolean answer = controller.choiceListVehicleWaiting();
                if (answer) {
                }else{
                    JOptionPane.showMessageDialog(panel, controller.getErrorMessage(), "Search error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (UnknownHostException e) {
                JOptionPane.showMessageDialog(panel, "Server is unreachable (Unknown Host)", "Connection error", JOptionPane.ERROR_MESSAGE);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(panel, e.getMessage(), "Connection error", JOptionPane.ERROR_MESSAGE);
            } catch (ParseException ex) {
                //To do
                Logger.getLogger(ReparatorMenuWindow.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
