package View;

import javax.swing.JFrame;
import CommunicationModel.Configuration;

public class main {
    /**
     * Declaration of attribute
     * @param config Object Configuration
     */
    final public static Configuration Config = new Configuration();
    
    /**
     * Display view connectWindow
     * @param args 
     */
    public static void main(String[] args){
        JFrame window = new ConnectWindow();
    }
}
