/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CommunicationModel;

import Model.UserDAO;
import java.io.IOException;
import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pds_user
 */
public class CommunicationTest {
    
    private JsonObject json = null;
    
    public CommunicationTest() {
    }
    
    @Before
    public void setUp(){
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "Test");
        json = builder.build();
    }
    

    /**
     * Test of sendData method and test of getData method, of class Communication.
     */
    @Test
    public void testSendAndGetData() throws Exception {
        System.out.println("sendData");
        /*UserDAO user = new UserDAO();
        json = user.getLoginRequestJson("csanterre","mdp");*/
        Communication instance = new Communication();
        instance.sendData(json);
        JsonObject result = instance.getData();
        JsonObject expResult = null;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "None");
        builder.add("State", "Action not found");
        expResult = builder.build();
        assertEquals(result, expResult);
    }


    /**
     * Test of close method, of class Communication.
     */
    @Test 
    public void testClose() throws IOException{
        System.out.println("close");
        Communication instance = new Communication();
        instance.close();
        try{
            instance.sendData(json);
        }catch(IOException e){
            assertNotNull(e);
        }
    }


    
}
