/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pds_user
 */
public class CarTest {
    
    public CarTest() {
    }

    /**
     * Test of serialize method, of class Car.
     */
    @Test
    public void testSerialize() throws ParseException {
        System.out.println("serialize");
        Car car = new Car (0,"123 AB 123",new DateFormatted("1995-05-17"),new DateFormatted("1996-10-03"));
        String expResult = "0///123 AB 123///1995-05-17///1996-10-03";
        String result = Car.serialize(car);
        assertEquals(expResult, result);
    }

    /**
     * Test of deserialize method, of class Car.
     */
    @Test
    public void testDeserialize() throws ParseException {
        System.out.println("deserialize");
        Car expResult = new Car (0,"123 AB 123",new DateFormatted("1995-05-17"),new DateFormatted("1996-10-03"));
        String serializeCar = "0///123 AB 123///1995-05-17///1996-10-03";
        Car result= Car.deserialize(serializeCar);
        assertEquals(expResult.toString(),result.toString());
    }
    
}
