/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author quentin_goutte
 */
public class ParkingTest {
    
    public ParkingTest() {
    }

    /**
     * Test of serialize method, of class Parking.
     */
    @Test
    public void testSerialize() {
        System.out.println("serialize");
        Parking parking = new Parking (0,false);
        String expResult = "0///false";
        String result = Parking.serialize(parking);
        assertEquals(expResult, result);

    }

    /**
     * Test of deserialize method, of class Parking.
     */
    @Test
    public void testDeserialize() {
        System.out.println("deserialize");
        Parking expResult = new Parking(0,false);
        Parking result = Parking.deserialize("0///false");
        assertEquals(expResult.toString(), result.toString());
    }
    
}
