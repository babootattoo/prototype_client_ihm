/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import javax.json.JsonObject;
import javax.json.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pds_user
 */
public class ReparationFormDAOTest {
    
    public ReparationFormDAOTest() {
    }
    
    /**
     * Test of getReparationForm method, of class ReparationFormDAO.
     */
    @Test
    public void testGetReparationForm() throws Exception {
        System.out.println("getReparationForm");
        int id = 1;
        JsonObject result = ReparationFormDAO.getReparationForm(id);
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "giveReparationForm");
        builder.add("State", "Success");
        builder.add("ReparationForm", "1///Waiting reparation///normal///2017-01-09///2017-02-03///null///null");
        builder.add("Car", "1///45 RFS 64///2016-12-02///2017-02-07");
        builder.add("Bike", "null");
        builder.add("Parking", "1///true");
        JsonObject expResult = builder.build();
        assertEquals(expResult, result);
    }
    
}
