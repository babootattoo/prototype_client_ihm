/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author quentin_goutte
 */
public class ReparationFormTest {
    
    public ReparationFormTest() {
    }

    /**
     * Test of serializeAll method, of class ReparationForm.
     */
    @Test
    public void testSerializeAll() throws ParseException {
        System.out.println("serializeAll");
        Parking park=new Parking(1,true);
        Bike bike = null;
        Car car = new Car(1, "45 RFS 64", new DateFormatted("2016-12-02"), new DateFormatted("2017-02-07"));
        ReparationForm instance = new ReparationForm(1, "Waiting reparation", "normal", park , car, bike, new DateFormatted("2017-01-09") , new DateFormatted("2017-02-03"), "", "");
        Map<String, String> expResult = new HashMap<String, String>();
        expResult.put("ReparationForm","1///Waiting reparation///normal///2017-01-09///2017-02-03//////");
        expResult.put("Bike","null");
        expResult.put("Car", "1///45 RFS 64///2016-12-02///2017-02-07");
        expResult.put("Parking", "1///true");
        Map<String, String> result = ReparationForm.serializeAll(instance);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of deserializeAll method, of class ReparationForm.
     */
    @Test
    public void testDeserializeAll() throws Exception {
        System.out.println("deserializeAll");
        Parking park=new Parking(1,true);
        Bike bike = null;
        Car car = new Car(1, "45 RFS 64", new DateFormatted("2016-12-02"), new DateFormatted("2017-02-07"));
        ReparationForm expResult = new ReparationForm(1, "Waiting reparation", "normal", park , car, bike, new DateFormatted("2017-01-09") , new DateFormatted("2017-02-03"), null, null);
               
        String serialiazedReparationForm = "1///Waiting reparation///normal///2017-01-09///2017-02-03///null///null";
        String serialiazedBike = "null";
        String serialiazedCar = "1///45 RFS 64///2016-12-02///2017-02-07";
        String serialiazedParking = "1///true";
        ReparationForm result = ReparationForm.deserializeAll(serialiazedReparationForm,serialiazedCar,serialiazedBike,serialiazedParking);
        assertEquals(expResult.toString(), result.toString());
    }
    
}
