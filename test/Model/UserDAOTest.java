/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pds_user
 */
public class UserDAOTest {
    
    public UserDAOTest() {
    }    

    /**
     * Test of tryLogin method, of class UserDAO.
     */
    @Test
    public void testTryLogin() throws Exception {
        System.out.println("tryLogin");
        String login = "csanterre";
        String password = "mdp";
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "Connection");
        builder.add("State", "Success");
        builder.add("UserLogged", "1///reparateur///SANTERRE///Charles///82 rue des poulettes///Paris///75005///csanterre///csanterre@gmail.com///2016-11-05///7.5");
        JsonObject expResult = builder.build();
        JsonObject result = UserDAO.tryLogin(login, password);
        assertEquals(expResult, result);
    }
    
}
