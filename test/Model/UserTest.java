/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author quentin_goutte
 */
public class UserTest {
    
    public UserTest() {
    }

    /**
     * Test of serialize method, of class User.
     */
    @Test
    public void testSerialize() throws ParseException {
        System.out.println("serialize");
        User instance = new User(0,"Administrator","Quentin","Goutte","12 rue de Paris", "Paris", "75000", "quentin_goutte", "quentingoutte@test.com", new DateFormatted("2016-01-01"),7.5);
        String expResult = "0///0///Administrator///Quentin///Goutte///12 rue de Paris///Paris///75000///quentin_goutte///quentingoutte@test.com///2016-01-01///7.5";
        String result = User.serialize(instance); 
        assertEquals(expResult, result);
        
    }

    /**
     * Test of deserialize method, of class User.
     */
    @Test
    public void testDeserialize() throws Exception {
        System.out.println("deserialize");
        String user = "0///Administrator///Quentin///Goutte///12 rue de Paris///Paris///75000///quentin_goutte///quentingoutte@test.com///2016-01-01///7.5";
        User expResult = new User(0,"Administrator","Quentin","Goutte","12 rue de Paris", "Paris", "75000", "quentin_goutte", "quentingoutte@test.com", new DateFormatted("2016-01-01"),7.5);
        User result = User.deserialize(user);
        assertEquals(expResult.toString(), result.toString());
    }
    
}
