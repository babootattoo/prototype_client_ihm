/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Delete this test ?
 * @author pds_user
 */
public class VehicleDAOTest {
    
    public VehicleDAOTest() {
    }
    
    @Test
    public void testUpdateVehicleRequestJson() throws ParseException, IOException{
        System.out.println("updateUpdateVehicleRequestJson");
        Car aCar = new Car(1,"45 RFS 75",new DateFormatted("2016-12-02"),new DateFormatted("2017-02-07"));
        JsonObject result = VehicleDAO.updateVehicleWaitingForReparation(aCar);
        
        JsonObject expResult = null;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "updateVehicleReparation");
        builder.add("State", "Success");    
        expResult = builder.build();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testAddVehicleWaitingForReparation() throws ParseException, IOException{
        System.out.println("testAddVehicleWaitingForReparation");
        String matriculation = "77 CUV 77";
        DateFormatted PurchaseDate = new DateFormatted("2016-12-02");
        DateFormatted LastRevision = new DateFormatted("2016-12-02");

        JsonObject result = VehicleDAO.addVehicleWaitingForReparation(matriculation, PurchaseDate, LastRevision);
        
        JsonObject expResult = null;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "addVehicleReparation");
        builder.add("State", "Success");    
        expResult = builder.build();
        assertEquals(expResult, result);        
   
    }
  
    
    @Test
    public void testDeleteVehicleWaitingForReparation() throws ParseException, IOException {
        System.out.println("testdeleteVehicleRequestJson");
        Car aCar = new Car(3,"77 CUV 77",new DateFormatted("2016-12-02"),new DateFormatted("2017-02-07"));
        JsonObject result = VehicleDAO.deleteVehicleWaitingForReparation(aCar);
        
        JsonObject expResult = null;
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action", "deleteVehicleReparation");
        builder.add("State", "Success");    
        expResult = builder.build();
        assertEquals(expResult, result);        
    }

        /**
     * Test of getListVehicleWaitingForReparation method, of class VehicleDAO.
     */
    @Test
    public void testGetListVehicleWaitingForReparation() throws Exception {
        System.out.println("getListVehicleWaitingForReparation");
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("Action","giveListVehicleReparation");
        builder.add("State","Success");
        
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        JsonObjectBuilder carBuilder = Json.createObjectBuilder();
        
        carBuilder.add("Car0",Car.serialize(new Car(4,"80 ABC 05",new DateFormatted("2016-10-29"),new DateFormatted("2017-01-06"))));
        arrayBuilder.add(carBuilder);
        carBuilder.add("Car1",Car.serialize(new Car(5,"77 CUV 77",new DateFormatted("2016-12-02"),new DateFormatted("2016-12-02"))));
        arrayBuilder.add(carBuilder);
        carBuilder.add("Car2",Car.serialize(new Car(1,"45 RFS 75",new DateFormatted("2016-12-02"),new DateFormatted("2017-02-07"))));
        arrayBuilder.add(carBuilder);
        
        builder.add("ListCar",arrayBuilder);
        builder.add("ListBike","null");
        
        JsonObject expResult = builder.build();
        JsonObject result = VehicleDAO.getListVehicleWaitingForReparation();
        assertEquals(expResult, result);
    }

}
